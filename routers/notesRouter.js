const express = require('express');
const {asyncWrapper} = require('./helpers');
const {authMiddleware} = require('./middlewares/authMiddleware');
const {
  showAll,
  create,
  showOne,
  update,
  changeCompletedStatus,
  deleteNote,
} = require('../controllers/notesController');

const notesRouter = new express.Router();
notesRouter.use(authMiddleware);
notesRouter.get('/', asyncWrapper(showAll));
notesRouter.post('/', asyncWrapper(create));
notesRouter.get('/:id', asyncWrapper(showOne));
notesRouter.put('/:id', asyncWrapper(update));
notesRouter.patch('/:id', asyncWrapper(changeCompletedStatus));
notesRouter.delete('/:id', asyncWrapper(deleteNote));

module.exports = notesRouter;
