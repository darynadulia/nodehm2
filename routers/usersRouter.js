const express = require('express');
const {asyncWrapper} = require('./helpers');
const {authMiddleware} = require('./middlewares/authMiddleware');
const {
  showProfile,
  changePassword,
  deleteProfile,
} = require('../controllers/usersController');
const {changePasswordValidator} = require('./middlewares/validationMiddleware');

const usersRouter = new express.Router();
usersRouter.use(authMiddleware);
usersRouter.get('/me', showProfile);
usersRouter.patch('/me',
    asyncWrapper(changePasswordValidator),
    asyncWrapper(changePassword));
usersRouter.delete('/me', asyncWrapper(deleteProfile));

module.exports = usersRouter;
