const {Note} = require('../models/noteModel');

module.exports.showAll = async (req, res) => {
  const {offset, limit} = req.query;
  const notes = await Note.find(
      {userId: req.user._id},
      [],
      {
        skip: +offset,
        limit: +limit,
      },
  );
  res.json({notes});
};

module.exports.create = async (req, res) => {
  const {text} = req.body;

  if (!text) {
    return res.status(400).json({
      message: 'Please specify a text of your note',
    });
  }

  const note = new Note({
    userId: req.user._id,
    text,
  });
  await note.save();
  res.json({message: 'Success'});
};

module.exports.showOne = async (req, res) => {
  const id = req.params.id;
  const note = await Note.findById(id);
  if (!note) {
    return res.status(404).json({message: 'Not found'});
  }
  res.json({note});
};

module.exports.update = async (req, res) => {
  const id = req.params.id;
  const text = req.body.text;
  if (!text) {
    return res.status(400).json({message: 'You should specify a text'});
  }
  await Note.findByIdAndUpdate(id, {$set: {text}});
  res.json({message: 'Success'});
};

module.exports.changeCompletedStatus = async (req, res) => {
  const id = req.params.id;
  const {completed} = await Note.findById(id);
  await Note.findByIdAndUpdate(id, {$set: {completed: !completed}});
  res.json({message: 'Success'});
};

module.exports.deleteNote = async (req, res) => {
  await Note.findByIdAndDelete(req.params.id);
  res.json({message: 'Success'});
};
