const bcrypt = require('bcrypt');
const {User} = require('../models/userModel');

module.exports.showProfile = (req, res) => {
  res.json({user: req.user});
};

module.exports.changePassword = async (req, res) => {
  const {oldPassword, newPassword} = req.body;
  const user = await User.findOne({username: req.user.username});
  if (! (await bcrypt.compare(oldPassword, user.password))) {
    res.status(400).json({message: 'Wrong password'});
  }
  await User.findByIdAndUpdate(
      user._id,
      {$set: {password: await bcrypt.hash(newPassword, 10)}},
  );
  res.json({message: 'Success'});
};

module.exports.deleteProfile = async (req, res) => {
  await User.findByIdAndDelete(req.user._id);
  res.json({message: 'Success'});
};
